$(document).ready(function(){

    var btnStartPopup = $("#contact-vid");

    //CONTACT VIDEO POPUP
    btnStartPopup.click(function(cid){
        cid.preventDefault();
        $($(this).attr("href")).addClass("show");
    });
    $(document).click(function(e) {
        var $target = $(e.target);
        var popupContent = $(".popup");
        if (!$target.is(popupContent) && !$target.is(btnStartPopup) ) {
            if ($('.popup-overlay').hasClass('show')) {
              //$('#popup').hide();
              $(".popup-overlay").removeClass('show');
            }
        }
    });

    //FAQ
    $(".question").click(function(faq){
        faq.preventDefault();

        $($(this).next()).toggleClass("show");
        $($(this).children("i")).toggleClass("fa-angle-up fa-angle-down");
    });

    //GALLERY FILTER TAB
    $(".filter-tab li a").click(function(ft){
        ft.preventDefault();

        $(".filter-tab li").removeClass("active");
        $($(this).parent()).addClass("active")
        
        $(".grid-2 .grid-item").hide();
        $(".grid-2 .grid-item." + $(this).attr("data-filter")).show();
    });

    //STICKY NAV
    $(window).scroll(function(){
        if ($(window).scrollTop() >= 300) {
            $('.av-header').addClass('sticky');
        }
        else {
            $('.av-header').removeClass('sticky');
        }
    });

    //BURGER
    $(".burger").click(function(b){
        b.preventDefault();

        $($(this).children("i")).toggleClass("fa-times fa-bars");
        $(".av-main-nav").toggleClass("show");
    });

    //MOBILE DROPDOWN MENU
    if($(window).innerWidth() < 769){
        $(".av-main-nav li i").click(function(i){
            i.preventDefault();
            $($(this).parent()).siblings().toggleClass("show");
        });
    }
});